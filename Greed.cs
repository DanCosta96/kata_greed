using System;
using System.Collections;
using System.Collections.Generic;

public class Greed
{
    public int Points { get; private set; }
    public List<int> rollList { get; private set; }
    public int RolledPairs { get; private set; }
    public int StraightCount { get; private set; }
    
    public Greed()
    {
        Points = 0;
        RolledPairs = 0;
        StraightCount = 0;
    }

    public void Roll(List<int> rollList)
    {
        if (rollList.Count > 6)
            throw new ArgumentOutOfRangeException("Cannot Roll more than 6 dice");
        if (rollList.Count == 0)
            throw new ArgumentOutOfRangeException("Cannot Roll 0 dice");


        this.rollList = rollList;
        CheckRoll();
    }

    private void CheckRoll()
    {
        for (int i = 1; i <= 6; i++)
        {
            CheckNumber(number: i);
        }
        CheckThreePairs();
        CheckStraight();
    }

    private void CheckStraight()
    {
        if (StraightCount == 6)
        {
            Points = 1200;
        }
    }

    private void CheckThreePairs()
    {
        if (RolledPairs == 3)
            Points += 800;
    }

    public void CheckNumber(int number)
    {
        int timesRollRepeated = TimesRepeated(number);
        if (timesRollRepeated == 0)
            return;
        AddPoints(number, timesRollRepeated);
    }

    private void AddPoints(int number, int timesRollRepeated)
    {
        switch (timesRollRepeated)
        {
            case 6:
                AddSextetPoints(number);
                break;
            case 5:
                AddQuintetPoints(number);
                break;
            case 4:
                AddQuartetPoints(number);
                break;
            case 3:
                AddTripletsPoints(number);
                break;
            case 2:
                RolledPairs++;
                break;
            case 1:
                AddSinglePoints(number);
                break;
            default:
                break;
        }
    }

    private int GetTripletPoints(int diceRoll)
    {
        switch (diceRoll)
        {
            case 1:
                return 1000;
            case 2:
                return 200;
            case 3:
                return 300;
            case 4:
                return 400;
            case 5:
                return 500;
            case 6:
                return 600;
            default:
                return 0;
        }
    }

    private void AddSextetPoints(int number)
    {
        Points += GetTripletPoints(number) * 8;
    }

    private void AddQuintetPoints(int number)
    {
        Points += GetTripletPoints(number) * 4;
    }

    private void AddQuartetPoints(int number)
    {
        Points += GetTripletPoints(number) * 2;
    }

    private void AddTripletsPoints(int diceRoll)
    {
        Points += GetTripletPoints(diceRoll);
    }

    private void AddSinglePoints(int diceRoll)
    {
        StraightCount++;
        switch (diceRoll)
        {
            case 1:
                Points += 100;
                break;
            case 5:
                Points += 50;
                break;
            default:
                break;
        }
    }

    public int TimesRepeated(int diceRoll)
    {
        int timesRolled = 0;
        for (int i = 0; i < rollList.Count; i++)                
        { 
            if (rollList[i] == diceRoll)
                timesRolled++;
        }

        return timesRolled;
    }
}
