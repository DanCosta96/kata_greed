using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;

[TestFixture]
public class GreedTests
{
    public Greed greed;

    [SetUp]
    public void SetUp()
    {
        GivenAGreed();
    }

    [Test]
    public void RollASingleOneIs100Points()
    {
        WhenRoll(1,0,0,0,0);

        ThenPoints(100);
    }

    [Test]
    public void RollASingleFiveIs50Points()
    {
        WhenRoll(0, 0, 0, 0, 5);

        ThenPoints(50);
    }

    [Test]
    public void RollTripleOnesIs1000Points()
    {
        WhenRoll(1, 1, 1, 0, 0);

        ThenPoints(1000);
    }

    [Test]
    public void RollTripleTwosIs200Points()
    {
        WhenRoll(2, 2, 0, 0, 2);

        ThenPoints(200);
    }

    [Test]
    public void RollTripleThreesIs300Points()
    {
        WhenRoll(0, 0, 3, 3, 3);

        ThenPoints(300);
    }

    [Test]
    public void RollTripleFourIs400Points()
    {
        WhenRoll(0, 4, 0, 4, 4);

        ThenPoints(400);
    }

    [Test]
    public void RollTripleFiveIs500Points()
    {
        WhenRoll(5, 0, 5, 0, 5);

        ThenPoints(500);
    }

    [Test]
    public void RollTripleSixIs600Points()
    {
        WhenRoll(0, 6, 6, 6, 0);

        ThenPoints(600);
    }

    [Test]
    [TestCase(1,1,1,5,1,2050)]
    [TestCase(2,3,4,6,2,0)]
    [TestCase(3,4,5,3,3,350)]
    public void RollExamples(int a, int b, int c, int d, int e, int result)
    {
        WhenRoll(a,b,c,d,e);

        ThenPoints(result);
    }

    [Test]
    public void CanRoll1Dice()
    {
        WhenRoll(1);

        ThenPoints(100);

    }

    [Test]
    public void CanRoll6Dice()
    {
        WhenRoll(1,1,1,5,5,5);

        ThenPoints(1500);

    }

    [Test]
    public void RollFourOfAKindMultipliesTripletScoreBy2()
    {
        WhenRoll(2,2,2,2);

        ThenPoints(400);
    }

    [Test]
    public void RollFiveOfAKindMultipliesTripletScoreBy4()
    {
        WhenRoll(2, 2, 2, 2, 2);

        ThenPoints(800);
    }

    [Test]
    public void RollSixOfAKindMultipliesTripletScoreBy8()
    {
        WhenRoll(2,2,2,2,2,2);

        ThenPoints(1600);
    }

    [Test]
    public void RollThreePairsIs800Points()
    {
        WhenRoll(2, 2, 3, 3, 4, 4);

        ThenPoints(800);
    }

    [Test]
    public void RollStraightIs1200Points()
    {
        WhenRoll(1, 2, 3, 4, 5, 6);

        ThenPoints(1200);
    }

    [Test]
    public void CannotRollMoreThan6Dice()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => WhenRoll(1, 2, 3, 4, 5, 6, 7));
    }

    [Test]
    public void CannotRoll0Dice()
    {
        Assert.Throws<ArgumentOutOfRangeException>(() => greed.Roll(new List<int>()));
    }

    public void GivenAGreed()
    {
        greed = new Greed();
    }

    public void WhenRoll(params int [] rolledList)
    {
        greed.Roll(rolledList.OfType<int>().ToList());
    }

    public void ThenPoints(int expectedPoints)
    {
         Assert.That(greed.Points, Is.EqualTo(expectedPoints));
    }


}
